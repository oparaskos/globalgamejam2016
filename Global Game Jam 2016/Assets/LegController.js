var health = 100;

var giblet : GameObject;

var gibletSprite1 : Sprite;
var gibletSprite2 : Sprite;
var gibletSprite3 : Sprite;
var gibletSprite4 : Sprite;

function OnCollisionEnter2D(collision: Collision2D) {
  SendMessageUpwards("Landed");
}

function ApplyDamage(damageAmount : int) {
	health -= damageAmount;
	if(health <= 0)
		Explode();
}

function Explode() {
	var gibletSprites = [gibletSprite1, gibletSprite2, gibletSprite3, gibletSprite4];
	for(var giblet_sprite : Sprite in gibletSprites) {
		if(giblet_sprite == null)
			continue;
		var giblet_clone = Instantiate(giblet, transform.position, transform.rotation);
		var giblet_clone_sprite_renderer = giblet_clone.GetComponent.<SpriteRenderer>();
		giblet_clone_sprite_renderer.sprite = giblet_sprite;
	}
	Destroy(gameObject);
}
