﻿#pragma strict

var jumpingManuverability = 5;
var walkVelocity = 10;
var jumpImpulse = 10;

var health = 100;

var state = "Falling";

var rb: Rigidbody2D;


function Start () {
	rb = GetComponent.<Rigidbody2D>();
}

function BeginFalling() {
	state = "Falling";
	
}

function BeginWalking() {
	state = "Walking";
	
}

function BeginDead() {
	state = "Dead";
	Debug.Log("Dieded");
}

function BeginJumping() {
	rb.AddForce(Vector2.up * jumpImpulse, ForceMode2D.Impulse);
	state = "Jumping";
}

function OnCollisionEnter2D(collision: Collision2D) {
  switch(state) {
  	case "Falling":
  		BeginWalking();
  		break;
		default:
  		break;
  }
}


function UpdateWalking() {
	if(Input.GetAxis("Horizontal") < -0.1 || Input.GetAxis("Horizontal") > 0.1)
	{
		var walkFactor = Input.GetAxis("Horizontal") * walkVelocity;
		transform.Translate(Vector2.right * walkFactor * Time.deltaTime, Space.World);
	}
	if(Input.GetButton("Jump")) {
		BeginJumping();
	}
}


function UpdateDead() {
}


function UpdateFalling() {
	if(Input.GetAxis("Horizontal") < -0.1 || Input.GetAxis("Horizontal") > 0.1)
	{
		var walkFactor = Input.GetAxis("Horizontal") * jumpingManuverability;
		transform.Translate(Vector2.right * walkFactor * Time.deltaTime, Space.World);
	}
}


function UpdateJumping() {
	if(rb.velocity.y < 0.1)
		BeginFalling();
	if(Input.GetAxis("Horizontal") < -0.1 || Input.GetAxis("Horizontal") > 0.1)
	{
		var walkFactor = Input.GetAxis("Horizontal") * jumpingManuverability;
		transform.Translate(Vector2.right * walkFactor * Time.deltaTime, Space.World);
	}
}

function ApplyDamage(damageAmount : int) {
	health -= damageAmount;
	if(health <= 0)
		BeginDead();
}

function FixedUpdate () {
	switch(state) {
	case "Falling":
		UpdateFalling();
		break;
	case "Walking":
		UpdateWalking();
		break;
	case "Jumping":
		UpdateJumping();	
		break;
	case "Dead":
		UpdateDead();	
		break;
	}
}
