﻿#pragma strict

var spinSpeed = 30;
var damageAmount = 50;
var repulseImpulse = 20;

function Start () {

}

function OnCollisionEnter2D(collision: Collision2D) {
	if (collision.gameObject.tag == "Player") {
		collision.gameObject.SendMessage("ApplyDamage", damageAmount);
		var impulseDirection = Vector3.Normalize(collision.gameObject.transform.position - transform.position);
		collision.gameObject.GetComponent.<Rigidbody2D>().AddForce(impulseDirection * repulseImpulse, ForceMode2D.Impulse);
	}
}

function Update () {
	transform.Rotate (Vector3.forward * spinSpeed);
}
